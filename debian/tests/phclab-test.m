#!/usr/bin/env -S octave -W

pkg load phclab

## turn off warning about obsolete findstr function
## https://github.com/janverschelde/PHCpack/issues/45
warning('off', 'Octave:legacy-function');

## we adapt the test code from section 3 "Automatic Testing and Benchmarking"
## from the poster "PHClab: A MATLAB/Octave interface to PHCpack"
## http://homepages.math.uic.edu/~jan/PHClab_poster.pdf
f = {'ku10', 'cyclic5', 'fbrfive4', 'game4two'};
expected = [2 70 36 9];
for k = 1:4
  p = read_system(['/usr/share/doc/phcpack/examples/' f{k}]);
  t0 = clock;
  s = solve_system(p);
  et = etime(clock(),t0);
  n = size(s,2);
  fprintf('Found %d sols for %s in %f sec.\n', n, f{k}, et);
  assert(n == expected(k));
end;
